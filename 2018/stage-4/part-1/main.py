import numpy as np
import re

myfile = open('input-data.txt', 'r')
contents = myfile.read().strip().splitlines()
myfile.close()

# contents = [
#   '[1518-11-01 00:00] Guard #10 begins shift',
#   '[1518-11-01 00:05] falls asleep',
#   '[1518-11-01 00:25] wakes up',
#   '[1518-11-01 00:30] falls asleep',
#   '[1518-11-01 00:55] wakes up',
#   '[1518-11-01 23:58] Guard #99 begins shift',
#   '[1518-11-02 00:40] falls asleep',
#   '[1518-11-02 00:50] wakes up',
#   '[1518-11-03 00:05] Guard #10 begins shift',
#   '[1518-11-03 00:24] falls asleep',
#   '[1518-11-03 00:29] wakes up',
#   '[1518-11-04 00:02] Guard #99 begins shift',
#   '[1518-11-04 00:36] falls asleep',
#   '[1518-11-04 00:46] wakes up',
#   '[1518-11-05 00:03] Guard #99 begins shift',
#   '[1518-11-05 00:45] falls asleep',
#   '[1518-11-05 00:55] wakes up'
# ]
contents.sort()

def get_guard(g):
  return int(re.search('#([0-9]+)', g).group(1))

def get_minute(i):
  return int(re.search(':([0-9]+)]', i).group(1))

MINS = 60

guard_dict = {}
asleep = False
g = ''
shift_data = []
shift_started = False
start_sleep = 0
end_sleep = 0
for instr in contents:
  if 'begins' in instr:
    # New shift
    if shift_started:
      if asleep:
        shift_data.append(range(start_sleep, 60))
      asleep = False
      if g in guard_dict:
        guard_dict[g].append(shift_data)
      else:
        guard_dict[g] = [shift_data]
    g = get_guard(instr)
    shift_data = []
    shift_started = True
  elif 'falls asleep' in instr:
    asleep = True
    start_sleep = get_minute(instr)
  elif 'wakes up' in instr:
    end_sleep = get_minute(instr)
    shift_data.append(range(start_sleep, end_sleep))
    asleep = False

# Do the last one
if g in guard_dict:
  guard_dict[g].append(shift_data)
else:
  guard_dict[g] = [shift_data]

# Find guard with most minutes asleep
max_guard = 0
max_minutes = 0
for guard in guard_dict:
  flat_list = [item for sublist in guard_dict[guard] for item in sublist]
  value = sum(map(lambda d: len(d), flat_list))
  if value > max_minutes:
    max_minutes = value
    max_guard = guard

# Get when most asleep
flat_list = [item for sublist in guard_dict[max_guard] for item in sublist]
flat_list = [item for sublist in flat_list for item in sublist]
mode = max(set(flat_list), key=flat_list.count)

print(max_guard * mode)