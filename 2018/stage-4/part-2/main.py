import numpy as np
import re
from collections import defaultdict

myfile = open('input-data.txt', 'r')
contents = myfile.read().strip().splitlines()
myfile.close()

# contents = [
#   '[1518-11-01 00:00] Guard #10 begins shift',
#   '[1518-11-01 00:05] falls asleep',
#   '[1518-11-01 00:25] wakes up',
#   '[1518-11-01 00:30] falls asleep',
#   '[1518-11-01 00:55] wakes up',
#   '[1518-11-01 23:58] Guard #99 begins shift',
#   '[1518-11-02 00:40] falls asleep',
#   '[1518-11-02 00:50] wakes up',
#   '[1518-11-03 00:05] Guard #10 begins shift',
#   '[1518-11-03 00:24] falls asleep',
#   '[1518-11-03 00:29] wakes up',
#   '[1518-11-04 00:02] Guard #99 begins shift',
#   '[1518-11-04 00:36] falls asleep',
#   '[1518-11-04 00:46] wakes up',
#   '[1518-11-05 00:03] Guard #99 begins shift',
#   '[1518-11-05 00:45] falls asleep',
#   '[1518-11-05 00:55] wakes up'
# ]
contents.sort()

def get_guard(i):
  return int(re.search('#([0-9]+)', i).group(1))

def get_minute(i):
  return int(re.search(':([0-9]+)]', i).group(1))

start_sleep = 0
guard_dict = defaultdict(int)
for instr in contents:
  time = get_minute(instr)
  if 'begins' in instr:
    # New shift
    guard = get_guard(instr)
  elif 'falls asleep' in instr:
    start_sleep = time
  elif 'wakes up' in instr:
    for t in range(start_sleep, time):
      guard_dict[(guard, t)] += 1

# Find guard of most frequent minute
max_val = 0
max_guard = 0
for v in guard_dict:
  if guard_dict[v] > max_val:
    max_val = guard_dict[v]
    max_guard = v

print(max_guard[0] * max_guard[1])