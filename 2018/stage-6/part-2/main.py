import numpy as np
import re

myfile = open('input-data.txt', 'r')
contents = myfile.read().strip().splitlines()
myfile.close()

# contents = [
#   '1, 1',
#   '1, 6',
#   '8, 3',
#   '3, 4',
#   '5, 5',
#   '8, 9',
# ]
MAX = 10000

contents = np.array(list(map(lambda x: [int(x.split(', ')[0]), int(x.split(', ')[1])], contents)))
xmax, ymax = contents.max(axis=0)

all_points = list(range(1, len(contents) + 1))
finite_points = list(range(1, len(contents) + 1))

grid = np.zeros((xmax + 1, ymax + 1))

def calc_manhatten(start, end):
  return sum(abs((end - start)))

for x in range(xmax + 1):
  for y in range(ymax + 1):
    dist = 0
    for i, p in enumerate(contents):
      dist += calc_manhatten(p, np.array([x, y]))
    grid[x][y] = 1 if dist < MAX else 0

print(int(sum(grid.flatten())))

# Calculate edge values
# vals = list(grid[0])+ list(grid[xmax]) + list(grid[:,0])+ list(grid[:,ymax])

# for v in vals:
#   v = int(v)
#   if v in finite_points:
#     finite_points.remove(v)

def sum_points(point, igrid):
  return sum((igrid == point).flatten())

# max_val = 0
# for p in finite_points:
#   val = sum_points(p, grid)
#   if val > max_val:
#     max_val = val

