package main

import (
  "bufio"
  "fmt"
  "log"
	"os"
	"strconv"
	"strings"
	"runtime"
	"path/filepath"
)

// readLines reads a whole file into memory
// and returns a slice of its lines.
func readLines(path string) ([]string, error) {
  file, err := os.Open(path)
  if err != nil {
    return nil, err
  }
  defer file.Close()

  var lines []string
  scanner := bufio.NewScanner(file)
  for scanner.Scan() {
    lines = append(lines, scanner.Text())
  }
  return lines, scanner.Err()
}

func convertToInt(s string) int {
	// First strip whitespace and leading + if present
	s = strings.TrimSpace(s)
	s = strings.TrimPrefix(s, "+")

	// string to int
	i, err := strconv.Atoi(s)
	if err != nil {
			// handle error
			fmt.Println(err)
			os.Exit(2)
	}
	return i
}

func loadData(name string) []string {
	_, dir, _, _ := runtime.Caller(0)
	dir = filepath.Dir(dir)
	dataPath := fmt.Sprintf("%s/../%s", dir, name)
	lines, err := readLines(dataPath)
	if err != nil {
		log.Fatalf("readLines: %s", err)
	}
	return lines
}

func main() {
	lines := loadData("input-data.txt")
	var value int
  for _, line := range lines {
    value += convertToInt(line)
	}
	fmt.Println("Value is: ", value)
}