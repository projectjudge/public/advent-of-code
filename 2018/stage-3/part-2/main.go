package main

import (
  "bufio"
  "fmt"
  "log"
	"os"
	"strconv"
	"strings"
)

// readLines reads a whole file into memory
// and returns a slice of its lines.
func readLines(path string) ([]string, error) {
  file, err := os.Open(path)
  if err != nil {
    return nil, err
  }
  defer file.Close()

  var lines []string
  scanner := bufio.NewScanner(file)
  for scanner.Scan() {
    lines = append(lines, scanner.Text())
  }
  return lines, scanner.Err()
}

func convertToInt(s string) int {
	// First strip whitespace and leading + if present
	s = strings.TrimSpace(s)
	s = strings.TrimPrefix(s, "+")

	// string to int
	i, err := strconv.Atoi(s)
	if err != nil {
			// handle error
			fmt.Println(err)
			os.Exit(2)
	}
	return i
}

func seenBefore(v int, m map[int]int) bool {
	if m[v] != 0 {
		return true
	}
	m[v] = 1
	return false
}

func nextPos(pos int, length int) int {
	if pos == length -1 {
		return 0
	}
	return pos + 1
}

func main() {
	changes, err := readLines("input-data.txt")
	// changes := []string{"+7", "+7", "-2", "-7", "-4"}
  if err != nil {
    log.Fatalf("readLines: %s", err)
	}

	seenFreqs := make(map[int]int)
	var freq int
	var pos int
  for true {
		freq += convertToInt(changes[pos])
		if seenBefore(freq, seenFreqs) {
			break
		}
		pos = nextPos(pos, len(changes))
	}
	fmt.Println("Freq first seen twice is: ", freq)
}