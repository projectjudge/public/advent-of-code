import numpy as np

myfile = open('input-data.txt', 'r')
contents = myfile.read().strip().splitlines()
myfile.close()

def getValues(val):
  val = val.split(' ')
  xstart = int(val[2].strip(':').split(',')[0])
  ystart = int(val[2].strip(':').split(',')[1])
  xlen = int(val[3].split('x')[0])
  ylen = int(val[3].split('x')[1])
  return xstart, ystart, xlen, ylen


max_width = 1000
# contents = [
#   '#1 @ 1,3: 4x4',
#   '#2 @ 3,1: 4x4',
#   '#3 @ 5,5: 2x2'
# ]
matrix = np.zeros((max_width, max_width))

for i in contents:
  xstart, ystart, xlen, ylen = getValues(i)
  for x in range(xstart, xstart + xlen):
    for y in range(ystart, ystart + ylen):
      matrix[x][y] += 1

# overlapping_matrix = (matrix > 0) * 1
# print(overlapping_matrix)

# current_val = print(sum((matrix > 1).flatten()))
# step = 1
for i in contents:
  # temp = np.copy(matrix)
  xstart, ystart, xlen, ylen = getValues(i)
  minimum = xlen * ylen
  values = []
  for x in range(xstart, xstart + xlen):
    for y in range(ystart, ystart + ylen):
      values.append(matrix[x][y])
  if sum(values) == minimum:
    print('--------------')
    print(i)
  # print(temp)
  # for x in range(xstart, xstart + xlen):
  #   for y in range(ystart, ystart + ylen):
  #     temp[x][y] += 1
  # print(temp, sum((temp > 1).flatten()))
  # if sum((temp > 1).flatten()) > xlen * ylen:
  #   print('--------------------')
  #   print(i)
  #   break
  

# print(matrix)
# print(matrix > 1)
# print(sum((matrix > 1).flatten()))
