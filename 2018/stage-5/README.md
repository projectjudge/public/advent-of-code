# Instructions

## From repo root

```bash
go run main.go
```

## From year/stage

```bash
cd <>/<>/<>
go run <part>/main.go
```