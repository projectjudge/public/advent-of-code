package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strings"
)

// readLines reads a whole file into memory
// and returns a slice of its lines.
func readLines(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, scanner.Err()
}

func loadData(name string) []string {
	_, dir, _, _ := runtime.Caller(0)
	dir = filepath.Dir(dir)
	dataPath := fmt.Sprintf("%s/../%s", dir, name)
	lines, err := readLines(dataPath)
	if err != nil {
		log.Fatalf("readLines: %s", err)
	}
	return lines
}

// destroy checks whether the values will be destroyed
func destroy(first string, second string) bool {
	if first != second {
		if strings.ToLower(first) == strings.ToLower(second) {
			return true
		}
	}
	return false
}

func calcLen(polymer string) int {
	pos := 0
	for pos < len(polymer)-1 {
		first, second := string(polymer[pos]), string(polymer[pos+1])
		if destroy(first, second) {
			polymer = strings.Replace(polymer, first+second, "", 1)
			if pos == 0 {
				pos = 0
			} else {
				pos--
			}
			continue
		}
		pos++
	}
	return len(polymer)
}

// stripLetter strips letter from polymer
func stripLetter(letter byte, polymer string) string {
	lower := strings.ToLower(string(letter))
	upper := strings.ToUpper(string(letter))
	newPolymer := strings.Replace(polymer, upper, "", -1)
	return strings.Replace(newPolymer, lower, "", -1)
}

// calcMinLen calculates the new minimum for
// a polymer after stripping out bad letters
func calcMinLen(polymer string, alphabet []byte) int {
	minLen := len(polymer)
	for _, letter := range alphabet {
		newPolymer := stripLetter(letter, polymer)
		newLen := calcLen(newPolymer)
		if newLen < minLen {
			minLen = newLen
		}
	}
	return minLen
}

func main() {
	polymer := loadData("input-data.txt")[0]

	alphabet := []byte("abcdefghijklmnopqrstuvwxyz")

	fmt.Printf("\nResult: %d\n", calcMinLen(polymer, alphabet))
}
