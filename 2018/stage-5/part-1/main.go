package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strings"
)

// readLines reads a whole file into memory
// and returns a slice of its lines.
func readLines(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, scanner.Err()
}

func loadData(name string) []string {
	_, dir, _, _ := runtime.Caller(0)
	dir = filepath.Dir(dir)
	dataPath := fmt.Sprintf("%s/../%s", dir, name)
	lines, err := readLines(dataPath)
	if err != nil {
		log.Fatalf("readLines: %s", err)
	}
	return lines
}

// destroy checks whether the values will be destroyed
func destroy(first string, second string) bool {
	if first != second {
		if strings.ToLower(first) == strings.ToLower(second) {
			return true
		}
	}
	return false
}

func calcLen(polymer string) int {
	pos := 0
	for pos < len(polymer)-1 {
		first, second := string(polymer[pos]), string(polymer[pos+1])
		if destroy(first, second) {
			polymer = strings.Replace(polymer, first+second, "", 1)
			if pos == 0 {
				pos = 0
			} else {
				pos--
			}
			continue
		}
		pos++
	}
	return len(polymer)
}

func main() {
	polymer := loadData("input-data.txt")[0]

	fmt.Printf("\nResult: %d\n", calcLen(polymer))
}
